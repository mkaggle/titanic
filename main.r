
cleansing <- function(D) {
    D$Age[is.na(D$Age)] <- median(D$Age, na.rm = TRUE)
    D$Fare[is.na(D$Fare)] <- median(D$Fare, na.rm = TRUE)
    D$Embarked[is.na(D$Embarked)] <- "S"

    D$SibSp <- D$SibSp + D$Parch
    D$Parch <- NULL

    D$Cabin <- NULL

    D$Sex <- as.factor(D$Sex)
    D$Pclass <- as.factor(D$Pclass)
    D$Embarked <- as.factor(D$Embarked)

    print(paste("the num of NA:", sum(is.na(D))))

    return(D)
}

train_raw <- read.csv("train.csv", na.strings = "")
test_raw <- read.csv("test.csv", na.strings = "")

explanatoryVariable <- c("Pclass", "Sex", "Age", "SibSp", "Parch", "Fare", "Embarked")

data.train <- cleansing(train_raw[, c(explanatoryVariable, "Survived")])
data.train$Survived <- as.factor(data.train$Survived)
data.test <- cleansing(test_raw[, explanatoryVariable])

                                        # Learn & Predict
library(caret)
fitControl <- trainControl(method = "repeatedcv",
                           number = 10,#10-fold
                           repeats=3,#3回繰り返す
                           selectionFunction = "oneSE")
grid <- expand.grid(mtry = 1 : 7)

rf <- train(Survived~.,
            data = data.train,
            method = "rf",
            trControl = fitControl,
            tuneGrid = grid)

p <- predict(rf, data.test)

write.table(data.frame("PassengerId" = test_raw$PassengerId, "Survived" = p), sep = ",", file = "predict.csv", quote = FALSE, row.names = FALSE, col.names = TRUE)
